# A General Two-Step Approach to Learning-Based Hashing

author: Guosheng Lin <guosheng.lin@adelaide.edu.au>

correspondence should be addressed to Chunhua Shen <chhshen@gmail.com>


__This is the implementation of the following paper. If you use this code in your research,
please cite our paper__

```
 @inproceedings{ICCV13Lin,
   author    = "Guosheng Lin and  Chunhua Shen and  David Suter and  Anton {van den Hengel}",
   title     = "A general two-step approach to learning-based hashing",
   booktitle = "IEEE Conference on Computer Vision (ICCV'13)",
   address   = "Sydney, Australia",
   year      = "2013",
 }
```
The paper can be downlaoded at <http://arxiv.org/abs/1309.1853>


## Notes
Please use the code of FastHash (<https://bitbucket.org/chhshen/fasthash/>) which includes the implementation of TSH.


## Install

Several toolboxes are required for running this code. For convenience, they are included in the folder: /libs
 and pre-compiled in Linux. These toolboxes are as follows:

- LBFGS-B solver is required for binary code inference, which can be downloaded at: <https://github.com/pcarbo/lbfgsb-matlab>.

- LIBLINEAR is required for learning linear SVM hash functions, which can be downloaded at: <http://www.csie.ntu.edu.tw/~cjlin/liblinear/>. After compiling, users should change the file name of train.mexxxx (e.g., train.mexa64 in Linux) in the folder: liblinear/matlab/ to liblinear_train.mexxxx.

- LIBSVM is required for learning RBF kernel SVM hash functions, which can be downloaded at: <http://www.csie.ntu.edu.tw/~cjlin/liblinear/>. After compiling, users should change the file name of svmtrain.mexxxx (e.g., svmtrain.mexa64 in Linux) in the folder: libsvm/matlab/ to libsvm_train.mexxxx.

- BudgetedSVM is required for learning RBF kernel SVM hash functions with a support vector budget, which can be downloaded at: <http://www.dabi.temple.edu/budgetedsvm/>. This toolbox includes an improved implementation of BSGD which is originally published at <http://www.dabi.temple.edu/~vucetic/BSGD.html>.



The file demo.m shows that how to use the code.


## Copyright

Copyright (c) Guosheng Lin, Chunhua Shen. 2013.

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.




